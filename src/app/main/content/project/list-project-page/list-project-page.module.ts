import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarModule } from '../../../../components/navbar/navbar.module';
import { Routes, RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatTableModule, MatMenuModule, MatIconModule, MatProgressBarModule, MatProgressSpinnerModule, MatSnackBarModule, MatButtonModule } from '@angular/material';
import { ListProjectPageComponent } from './list-project-page.component';
import { ProjectServiceModule } from '../../../services/project/project-service.module';

const routes: Routes = [
  { path: '', component: ListProjectPageComponent }
];

@NgModule({
  imports: [
    CommonModule,
    MatTableModule,
    FlexLayoutModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    RouterModule.forChild(routes),
    NavbarModule,
    ProjectServiceModule
  ],
  declarations: [ListProjectPageComponent],

})
export class ListProjectPageModule { }
