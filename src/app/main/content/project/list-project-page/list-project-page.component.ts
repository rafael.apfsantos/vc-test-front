import { Component, OnInit } from '@angular/core';
import { MatTableDataSource, MatSnackBar, MatDialogRef, MatDialog } from '@angular/material';
import { ProjectService } from '../../../services/project/project.service';

@Component({
  selector: 'app-list-project-page',
  templateUrl: './list-project-page.component.html',
  styleUrls: ['./list-project-page.component.css']
})
export class ListProjectPageComponent implements OnInit {


  /**
   * @var dataSourceTable
   */
  public dataSourceTable: MatTableDataSource<any>;

  /**
   * @var displayedColumnsTable
   */
  public displayedColumnsTable: Array<string> = [];

  /**
   * @var loadingDataSource
   */
  public loadingDataSource: Boolean = false;

  /**
   * Serviço de API para os posts.
   * @var projectService
   */
  private projectService: ProjectService;

  /**
   * @var snackBarMessage
   */
  private snackBarMessage: MatSnackBar;

  /**
   * constructor
   * @param projectService 
   * @param snackBarMessage 
   */
  constructor
  (
    projectService: ProjectService,
    snackBarMessage: MatSnackBar
  )
  {
    this.projectService = projectService;
    this.snackBarMessage = snackBarMessage
  }

  /**
   * ngOnInit
   */
  public ngOnInit(): void 
  {
    this.displayedColumnsTable = ['id', 'idProjectGitLab', 'name', 'description', 'import', 'action'];
    this.getProjectsImportedGitLab();
  }

  /**
   * getProjectsImportedGitLab
   */
  public getProjectsImportedGitLab(): void 
  {

    this.loadingDataSource = true;

    this.projectService.get().subscribe((response: any) => {

      if (typeof response !== 'undefined' && typeof response === 'object' && response.data.length > 0) {

        this.loadingDataSource = false;

        if (response.error == true) {
          this.snackBarMessage.open(response.data.message, 'OK', {
            panelClass: ['bg-danger-snack'],
            duration: 12000,
          });
        } else {
          this.dataSourceTable = new MatTableDataSource(response.data);
          this.snackBarMessage.open("Registros carregado com sucesso!", 'OK', {
            duration: 6000,
          });
        }
      } else {
        this.loadingDataSource = false;
        this.dataSourceTable = null;
        this.snackBarMessage.open("Erro ao carregar dados.", 'OK', {
          panelClass: ['bg-danger-snack'],
          duration: 12000,
        });
      }
    }, (error: any) => {
      this.loadingDataSource = false;
      this.dataSourceTable = null;
      this.snackBarMessage.open("Erro ao carregar dados.", 'OK', {
        panelClass: ['bg-danger-snack'],
        duration: 12000,
      });
    });
  }

  /**
   * importProjectsGitLab
   */
  public importProjectsGitLab(): void 
  {

    this.loadingDataSource = true;

    this.projectService.import().subscribe((response: any) => {

      if (typeof response !== 'undefined' && typeof response === 'object') {

        this.loadingDataSource = false;

        if (response.error == true) {
          this.snackBarMessage.open(response.data.message, 'OK', {
            panelClass: ['bg-danger-snack'],
            duration: 12000,
          });
        } else {
          this.getProjectsImportedGitLab();
        }
      } else {
        this.loadingDataSource = false;
        this.dataSourceTable = null;
        this.snackBarMessage.open("Erro ao importar dados.", 'OK', {
          panelClass: ['bg-danger-snack'],
          duration: 12000,
        });
      }
    }, (error: any) => {
      this.loadingDataSource = false;
      this.dataSourceTable = null;
      this.snackBarMessage.open("Erro ao importar dados.", 'OK', {
        panelClass: ['bg-danger-snack'],
        duration: 12000,
      });
    });
  }
}
