import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListProjectPageComponent } from './list-project-page.component';

describe('ListProjectPageComponent', () => {
  let component: ListProjectPageComponent;
  let fixture: ComponentFixture<ListProjectPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListProjectPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListProjectPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
