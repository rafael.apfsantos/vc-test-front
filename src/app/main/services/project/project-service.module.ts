import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TwcApiServiceModule } from '../../../core/services/tcw-api/twc-api.module';
import { ProjectService } from './project.service';

@NgModule({
  imports: [
    CommonModule,
    TwcApiServiceModule
  ],
  providers: [
    ProjectService
  ]
})
export class ProjectServiceModule { }
