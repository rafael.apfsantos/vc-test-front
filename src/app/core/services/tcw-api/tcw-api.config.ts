import { Injectable } from "@angular/core";

/**
 * Configurações de endpoint de API de acordo com o ambiente.
 */
const config = 
{
    'localhost' : {
        address: 'http://localhost:55924/api/'
    }
};

/**
 * Arquivo de configuração de URL da API.
 * TcwApiConfigService
 * @author Rafael Franco <rafael.apfsantos@gmail.com>
 */
@Injectable()
export class TcwApiConfigService
{
    public getConfig()
    {
        return config;
    }
}