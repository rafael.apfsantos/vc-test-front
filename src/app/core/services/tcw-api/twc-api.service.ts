
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { TcwApiConfigService } from './tcw-api.config';


/**
 * TwcApiService
 * 
 * Serviço de consumo da pos-api
 * @author Rafael Franco <rafael.apfsantos@gmail.com>
 */
@Injectable()
export class TwcApiService {
    /**
     * httpClient
     * 
     * Cliente Http que será utilizado para realizar as
     * requisições contra o servidor.
     */
    private httpClient: HttpClient;

    /**
     * tcwApiConfigService
     * 
     * Serviço de configuração da API do point-of-sale.
     */
    private tcwApiConfigService: TcwApiConfigService;

    /**
     * constructor
     * @param httpClient 
     * @param tcwApiConfigService 
     */
    public constructor(httpClient: HttpClient, tcwApiConfigService: TcwApiConfigService) {
        this.httpClient = httpClient;
        this.tcwApiConfigService = tcwApiConfigService;
    }

    /**
     * getApiAddress
     */
    public getApiAddress(): string {
        return this.tcwApiConfigService.getConfig()[window.location.hostname].address;
    }

    /**
     * getOptions
     */
    public getOptions(): any {

        let options = {};

        options = {
            headers: {
                Accept: 'application/json'
            }
        };

        return options;
    }

    /**
     * post
     * api/posts/store
     * @param apiAddress 
     * @param data 
     */
    public post(apiAddress: string, data:any): Observable<Object> 
    {
        return this.httpClient.post(this.getApiAddress() + apiAddress, data, this.getOptions());
    }

    /**
     * put
     * @param apiAddress 
     * @param id 
     * @param data 
     */
    public put(apiAddress: string, id: number, data: any): Observable<Object> 
    {
        return this.httpClient.put(this.getApiAddress() + apiAddress + "/" + id, data, this.getOptions());
    }

    /**
     * delete
     * @param apiAddress 
     * @param id 
     */
    public delete(apiAddress: string, id: number): Observable<Object> 
    {
        return this.httpClient.delete(this.getApiAddress() + apiAddress + "/" + id, this.getOptions());
    }

    /**
     * get
     * @param apiAddress 
     * @param data 
     */
    public get(apiAddress: string): Observable<Object> {
        return this.httpClient.get(this.getApiAddress() + apiAddress, this.getOptions());
    }
}